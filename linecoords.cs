
   // Returns a list of absolute coordinate in the shape of a line perpendicular to the LookAt vector
   public List<Vector2Int> LineCoordsFromCenter(Vector2Int originPosition, Vector2Int lookAt, int lengthPerDirection = 1) {
     Vector2Int targetPosition = originPosition + lookAt;
 
     List<Vector2Int> coords = new List<Vector2Int>();
     int leftXDirection;
     int leftYDirection;
 
     if (lookAt.y != 0) {
       leftXDirection = -1 * lookAt.y;
     }
     else {
       leftXDirection = 0;
     }
 
     if (lookAt.x != 0) {
       leftYDirection = 1 * lookAt.x;
     }
     else {
       leftYDirection = 0;
     }
 
     Vector2Int leftCoordsRelative = new Vector2Int(leftXDirection, leftYDirection);
     Vector2Int rightCoordsRelative = leftCoordsRelative * -1; 
 
     for (int i=1; i<=lengthPerDirection; i++) {
       coords.Add(targetPosition + leftCoordsRelative * i); 
       coords.Add(targetPosition + rightCoordsRelative * i); 
     }
 
     coords.Add(targetPosition);
 
     return coords;
   }
